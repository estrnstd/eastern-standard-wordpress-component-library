<?php
/*
Plugin Name: Eastern Standard WordPress Component Library
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-component-library
Description: SHOULD NOT BE ACTIVATED. Activating this plugin will enable all component library features for testing. Its purpose is to help determine the capabilities of a component library feature. This plugin is not something that should be relied on for real functionality of a site. After testing, copy the desired code out of this plugin into your theme, then deactivate this plugin.
Version: 1.2.0
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-component-library
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

#[AllowDynamicProperties]
class ESWP_Component_Library {

	public $plugin_data;
	public $name;
	public $slug;
	public $version;
	public $info_url;
	public $cache_key;
	public $cache_allowed;
	public $cache_length;
	public $directory_public;
	public $plugin_dependencies;

	public function __construct() {
		$this->plugin_data = get_plugin_data(__FILE__);
		$this->name = $this->plugin_data['Name'];
		$this->slug = plugin_basename(__DIR__);
		$this->version = $this->plugin_data['Version'];
		$this->allow_dashboard_updating = true;
		$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-component-library/raw/HEAD/info.json';
		$this->cache_key = $this->slug;
		$this->cache_allowed = true;
		$this->cache_length = DAY_IN_SECONDS;
		$this->directory = plugin_dir_path(__FILE__);
		$this->directory_public = '/wp-content/plugins/eswp-component-library/';
		$this->plugin_dependencies = [
			[
				'name' => 'Advanced Custom Fields Pro',
				'file' => 'advanced-custom-fields-pro/acf.php',
				'link' => 'https://www.advancedcustomfields.com/pro',
			],
			[
				'name' => 'Eastern Standard WordPress Utilities',
				'file' => 'eswp-utilities/eswp-utilities.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities',
			],
			[
				'name' => 'Eastern Standard WordPress Post Type Framework',
				'file' => 'eswp-post-type-framework/eswp-post-type-framework.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-type-framework',
			],
			[
				'name' => 'Eastern Standard WordPress Block Framework',
				'file' => 'eswp-block-framework/eswp-block-framework.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-block-framework',
			],
			[
				'name' => 'Eastern Standard WordPress Taxonomy Framework',
				'file' => 'eswp-taxonomy-framework/eswp-taxonomy-framework.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-taxonomy-framework',
			],
			[
				'name' => 'Eastern Standard WordPress Field Group Framework',
				'file' => 'eswp-field-group-framework/eswp-field-group-framework.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework',
			],
			[
				'name' => 'Eastern Standard WordPress Post List',
				'file' => 'eswp-post-list/eswp-post-list.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-list',
			],
		];

		if ($this->allow_dashboard_updating) {
			$this->update_hooks();
		}
		$this->admin_enqueue();
		$this->check_dependencies();

		add_action('init', function () {
			if (is_plugin_active('eswp-post-type-framework/eswp-post-type-framework.php')) {
				eswp_post_type_framework_initialize($this->directory . 'post-types');
			}
			if (is_plugin_active('eswp-taxonomy-framework/eswp-taxonomy-framework.php')) {
				eswp_taxonomy_framework_initialize($this->directory . 'taxonomies');
			}
			if (is_plugin_active('eswp-block-framework/eswp-block-framework.php')) {
				eswp_block_framework_initialize($this->directory . 'blocks');
			}
			if (is_plugin_active('eswp-field-group-framework/eswp-field-group-framework.php')) {
				eswp_field_group_framework_initialize($this->directory . 'field-groups');
			}
			if (is_plugin_active('eswp-post-list/eswp-post-list.php')) {
				eswp_post_lists_initialize($this->directory . 'post-lists');
			}
		});

	}

	// Updates

	public function update_hooks() {
		add_filter('plugins_api', [$this, 'plugins_api_filter'], 20, 3);
		add_filter('site_transient_update_plugins', [$this, 'site_transient_update_plugins_filter']);
		add_action('upgrader_process_complete', [$this, 'delete_cache'], 10, 2);
	}

	public function get_info() {
		$info = get_transient($this->cache_key);
		if ($info === false || !$this->cache_allowed) {
			$remote_info = wp_remote_get($this->info_url, [
				'timeout' => 10,
				'headers' => [
					'Accept' => 'application/json'
				],
			]);
			if (
				is_wp_error($remote_info)
				|| wp_remote_retrieve_response_code($remote_info) !== 200
				|| empty(wp_remote_retrieve_body($remote_info))
			) {
				return false;
			}
			$info = $remote_info;
			set_transient($this->cache_key, $remote_info, $this->cache_length);
		}
		$info = json_decode(wp_remote_retrieve_body($info));
		return $info;
	}

	public function plugins_api_filter($result, $action, $args) {
		// Do nothing if you're not getting plugin information right now.
		if($action !== 'plugin_information') {
			return $result;
		}
		// Do nothing if it's not our plugin.
		if($args->slug !== $this->slug) {
			return $result;
		}
		// Get updates.
		$info = $this->get_info();
		if (!$info) {
			return $result;
		}
		$result = new stdClass();
		$result->name = $info->name;
		$result->slug = $info->slug;
		$result->version = $info->version;
		$result->tested = $info->tested;
		$result->requires = $info->requires;
		$result->author = $info->author;
		$result->author_profile = $info->author_profile;
		$result->download_link = $info->download_url;
		$result->trunk = $info->download_url;
		$result->requires_php = $info->requires_php;
		$result->last_updated = $info->last_updated;
		$result->sections = [
			'description' => $info->sections->description,
			'installation' => $info->sections->installation,
			'changelog' => $info->sections->changelog
		];
		if (!empty($info->banners)) {
			$result->banners = [
				'low' => $info->banners->low,
				'high' => $info->banners->high
			];
		}
		return $result;
	}

	public function site_transient_update_plugins_filter($transient) {
		if (empty($transient->checked)) {
			return $transient;
		}
		$info = $this->get_info();
		if (
			$info
			&& version_compare($this->version, $info->version, '<')
			&& version_compare($info->requires, get_bloginfo('version'), '<')
			&& version_compare($info->requires_php, PHP_VERSION, '<')
		) {
			$result = new stdClass();
			$result->slug = $this->slug;
			$result->plugin = plugin_basename(__FILE__);
			$result->new_version = $info->version;
			$result->tested = $info->tested;
			$result->package = $info->download_url;
			$transient->response[$result->plugin] = $result;
		}
		return $transient;
	}

	public function upgrader_process_complete_action($upgrader_object, $options) {
		if ($options['action'] === 'update' && $options['type'] === 'plugin') {
			$this->delete_cache();
		}
	}

	public function delete_cache() {
		delete_transient($this->cache_key);
	}

	// Admin enqueue

	public function admin_enqueue() {
		add_action('admin_enqueue_scripts', function () {
			wp_enqueue_style($this->slug, $this->directory_public . 'admin.css');
		});
	}

	// Dependencies

	public function check_dependencies() {
		$missing_or_deactivated_dependencies = [];

		foreach ($this->plugin_dependencies as $plugin_dependency) {
			if (!is_plugin_active($plugin_dependency['file'])) {
				array_push($missing_or_deactivated_dependencies, $plugin_dependency);
			}
		}

		if (count($missing_or_deactivated_dependencies) > 0) {
			$this->deactivate();
			add_action('admin_notices', function() use($missing_or_deactivated_dependencies) {
				$notice_output = '<div class="wrap ' . $this->slug . '-admin-notice">';
					$notice_output .= 'Plugin: ' . $this->name . ' has been deactivated because it requires';
					if (count($missing_or_deactivated_dependencies) <= 1) {
						$dependency = $missing_or_deactivated_dependencies[0];
						$notice_output .= ' plugin: <a href="' . $dependency['link'] . '" target="_blank">';
							$notice_output .= $dependency['name'];
						$notice_output .= '</a> to be installed and activated.';
					}
					else {
						$notice_output .= 'the following plugins to be installed and activated:';
						$notice_output .= '<ul>';
							foreach ($missing_or_deactivated_dependencies as $dependency) {
								$notice_output .= '<li>';
									$notice_output .= '<a href="' . $dependency['link'] . '" target="_blank">';
										$notice_output .= $dependency['name'];
									$notice_output .= '</a>';
								$notice_output .= '</li>';
							}
						$notice_output .= '</ul>';
					}
				$notice_output .= '</div>';
				echo $notice_output;
			});
		}	

	}

	public function deactivate() {
		deactivate_plugins(plugin_basename(__FILE__));
		if (isset($_GET['activate'])) {
			unset($_GET['activate']);
		}
	}

}

new ESWP_Component_Library();
