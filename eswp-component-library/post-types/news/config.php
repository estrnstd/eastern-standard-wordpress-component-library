<?php

/**
 * Post Type Config.
 *
 * @param  string  $post_type_directory  A path to this post type directory (includes trailing '/').
 */

// https://developer.wordpress.org/reference/functions/register_post_type

$singular_name = 'News';
$plural_name = 'News';

// Required
$eswp_post_type_data = [
	// Required
	'key' => 'news',
	// Required
	'arguments' => [
		'description' => 'A news item.',
		'hierarchical' => false,
		'taxonomies' => [],
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'rewrite' => [
			'slug' => 'news',
		],
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_rest' => true,
		'menu_position' => null,
		'menu_icon' => 'dashicons-media-document', // https://developer.wordpress.org/resource/dashicons
		'supports' => [
			// https://developer.wordpress.org/reference/functions/add_post_type_support
			'title',
			'editor',
			'excerpt',
		],
		'template' => [
			// [
			// 	'acf/image-hero'
			// ],
			[
				'acf/news-information'
			],
			// [
			// 	'acf/news-block-area'
			// ],
		],
		'template_lock' => 'all',
		'labels' => [
			// https://developer.wordpress.org/reference/functions/get_post_type_labels
			'name' => $plural_name,
			'singular_name' => $singular_name,
			'add_new_item' => "Add New {$singular_name}",
			'edit_item' => "Edit {$singular_name}",
			'new_item' => "New {$singular_name}",
			'view_item' => "View {$singular_name}",
			'view_items' => "View {$plural_name}",
			'search_items' => "Search {$plural_name}",
			'not_found' => "No {$plural_name} found",
			'not_found_in_trash' => "No {$plural_name} found in Trash",
			'parent_item_colon' => "Parent {$singular_name}:",
			'all_items' => "All {$plural_name}",
			'archives' => "{$singular_name} Archives",
			'attributes' => "{$singular_name} Attributes",
			'insert_into_item' => "Insert into {$singular_name}",
			'uploaded_to_this_item' => "Uploaded to this {$singular_name}",
			'filter_items_list' => "Filter {$plural_name} list",
			'items_list_navigation' => "{$plural_name} list navigation",
			'items_list' => "{$plural_name} list",
			'item_published' => "{$singular_name} published.",
			'item_published_privately' => "{$singular_name} published privately.",
			'item_reverted_to_draft' => "{$singular_name} reverted to draft.",
			'item_scheduled' => "{$singular_name} scheduled.",
			'item_updated' => "{$singular_name} updated.",
			'item_link' => "{$singular_name} Link",
			'item_link_description' => "A link to a {$singular_name}.",
		],
	],
	// Optional.
	'single-template' => $post_type_directory . 'single-news.php',
];
