const glob = require('glob');
const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');

const jsBundles = {
	blockScripts: {
		entryPath: './blocks/*/scripts/src/*.js',
		relativeDestPath: '../../dist/'
	},
	postListScripts: {
		entryPath: './post-lists/*/scripts/src/*.js',
		relativeDestPath: '../../dist/'
	}
};

const webpackConfigList = [];

for (const jsBundleName in jsBundles) {
	const jsBundle = jsBundles[jsBundleName];
	const jsEntries = glob.sync(jsBundle.entryPath);
	for (const jsEntryIndex in jsEntries) {
		const jsEntry = jsEntries[jsEntryIndex];
		const jsFileName = path.basename(jsEntry);
		const webpackConfig = {
			entry: jsEntry,
			target: 'web',
			output: {
				filename: jsFileName,
				path: path.resolve(jsEntry, jsBundle.relativeDestPath)
			},
			plugins: [
				new WebpackNotifierPlugin({onlyOnError: true}),
			],
			devtool: 'source-map',
			mode: 'production',
			watch: true
		};
		webpackConfigList.push(webpackConfig);
	}
}

module.exports = webpackConfigList;