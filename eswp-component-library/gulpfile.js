const gulp = require('gulp');
const glob = require('glob');
const path = require('path');
const sass = require('gulp-sass')(require('sass'));
const mergeStream = require('merge-stream');
const notify = require('gulp-notify');
const sourcemaps = require('gulp-sourcemaps');

const scssBundles = {
	blockStyles: {
		entryPath: './blocks/*/styles/src/*.scss',
		relativeDestPath: '../../dist/'
	},
	postListStyles: {
		entryPath: './post-lists/*/styles/src/*.scss',
		relativeDestPath: '../../dist/'
	}
};

const scssCompile = () => {

	const streams = [];

	for (const scssBundleName in scssBundles) {
		const scssBundle = scssBundles[scssBundleName];
		const scssEntries = glob.sync(scssBundle.entryPath);
		for (const scssEntryIndex in scssEntries) {
			const scssEntry = scssEntries[scssEntryIndex];
			const stream = gulp.src(scssEntry)
				.pipe(sass().on('error', sass.logError).on('error', notify.onError(`${scssBundleName} error in: ${scssEntry}`)))
				.pipe(sourcemaps.init())
				.pipe(sass({outputStyle: 'compressed'}))
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest(path.resolve(scssEntry, scssBundle.relativeDestPath)));
			streams.push(stream);
		}
	}

	if (streams.length > 0) {
		return mergeStream(...streams);
	}
	else {
		console.log(`No SCSS files found.`);
		return gulp.src('.', {allowEmpty: true});
	}

};

const watch = () => {
	for (const scssBundleName in scssBundles) {
		const scssBundle = scssBundles[scssBundleName];
		gulp.watch(scssBundle.entryPath, gulp.parallel(scssCompile));
		if (scssBundle.additionalWatchPath) {
			gulp.watch(scssBundle.additionalWatchPath, gulp.parallel(scssCompile));
		}
	}
};

exports.default = gulp.series(scssCompile, watch);