<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<div class="news-list">
	<?php eswp_post_list('news-list', [
		'query_string_filtering' => get_field('query_string_filtering'),
	]); ?>
</div>
