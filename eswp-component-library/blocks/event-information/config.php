<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory         A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'event-information';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Event Information'),
	'description' => __('Information about an event.'),
	'icon' => 'info-outline', // https://developer.wordpress.org/resource/dashicons
	'category' => 'post-structure',
	'keywords' => ['event', 'information'],
	'mode' => 'auto',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => false,
		'align' => false,
		'customClassName' => false,
		'reusable' => false
	],
	'post_types' => ['event'],
	'render_template' => $block_directory . $block_name . '.php',
];
