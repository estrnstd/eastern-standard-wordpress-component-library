<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<div class="event-information">
	<?php
		$type = get_field('type');
	?>
	<?php if ($type) : ?>
		<div>Type: <?php echo $type->name; ?></div>
	<?php endif; ?>
	<div>Start: <?php the_field('start_date'); ?></div>
	<div>End: <?php the_field('end_date'); ?></div>
</div>