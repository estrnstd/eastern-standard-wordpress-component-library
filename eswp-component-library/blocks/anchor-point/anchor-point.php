<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$anchor_point_name = get_field('name');
	$anchor_point_key = get_field('key');
	if ($anchor_point_name && !$anchor_point_key) {
		$anchor_point_key = strtolower($anchor_point_name);
		$anchor_point_key = str_replace(' ', '-', $anchor_point_key);
		$anchor_point_key = urlencode($anchor_point_key);
	}
?>

<div class="anchor-point" id="<?php echo $anchor_point_key; ?>">
	<?php if (!$anchor_point_name) : ?>
		<div class="eswp-error-text">Name is a required field.</div>
	<?php endif; ?>
	<?php if ($is_preview) : ?>
		<div class="eswp-help-text">Anchor points are only visible in the editor.</div>
		Anchor Point: <?php echo $anchor_point_name; ?>
	<?php endif; ?>
</div>