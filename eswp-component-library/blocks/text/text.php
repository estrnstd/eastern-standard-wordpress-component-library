<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$text = get_field('text');
?>

<div class="text">
	<?php if ($text) : ?>
		<?php the_field('text'); ?>
	<?php else : ?>
		<div class="eswp-error-text">Text is a required field.</div>
		<?php if ($is_preview) : ?>
			<div class="eswp-help-text">Click here to add text.</div>
		<?php endif; ?>
	<?php endif; ?>
</div>