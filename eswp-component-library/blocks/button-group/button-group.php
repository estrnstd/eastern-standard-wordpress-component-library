<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>
<div class="button-group-block">
	<?php if (have_rows('buttons')) : ?>
		<?php while (have_rows('buttons')) : ?>
		<?php
			the_row();

			$button_attributes = [
				'class' => 'button-group-block__button',
			];
			$button_text = '';

			$layout = get_row_layout();
			if ($layout) {
				$button_attributes['data-layout'] = $layout;
			}

			if ($layout === 'link') {
				$link = get_sub_field('link');
				if ($link && is_array($link)) {
					if (array_key_exists('title', $link) && is_string($link['title'])) {
						$button_text = $link['title'];
					}
					if (array_key_exists('url', $link) && is_string($link['url'])) {
						$button_attributes['href'] = $link['url'];
					}
					if (array_key_exists('target', $link) && is_string($link['target'])) {
						$button_attributes['target'] = $link['target'];
						$button_attributes['data-icon'] = $link['target'] == '_blank' ? 'new-tab' : '';
					}
				}
			}
			else if ($layout === 'download') {
				$button_attributes['data-icon'] = 'download';
				$button_attributes['download'] = '';
				$button_text = get_sub_field('button_text');
				$file = get_sub_field('file');
				if ($file && is_array($file)) {
					if (array_key_exists('url', $file) && is_string($file['url'])) {
						$button_attributes['href'] = $file['url'];
					}
					if (!$button_text) {
						if (array_key_exists('title', $file) && is_string($file['title'])) {
							$button_text = $file['title'];
							$button_attributes['download'] = $file['title'];
						}
						else if (array_key_exists('filename', $file) && is_string($file['filename'])) {
							$button_text = $file['filename'];
							$button_attributes['download'] = $file['filename'];
						}
					}
				}
			}
		?>

		<a<?php echo eswp_output_attributes($button_attributes); ?>><?php echo $button_text; ?></a>

		<?php endwhile; ?>
	<?php endif; ?>
</div>