<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'button-group';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Button Group'),
	'description' => __('A group of buttons.'),
	'icon' => 'button',
	'category' => 'common',
	'keywords' => ['button', 'group'],
	'mode' => 'auto',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false
	],
	'post_types' => [],
	'render_template' => $block_directory . 'button-group.php',
	'enqueue_assets' => function() use($block_name, $block_directory_public) {
		wp_enqueue_style($block_name, $block_directory_public . 'styles/dist/button-group.css');
	},
];
