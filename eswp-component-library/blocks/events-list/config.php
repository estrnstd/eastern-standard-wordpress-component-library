<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'events-list';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Events List'),
	'description' => __('A list of events.'),
	'icon' => 'calendar', // https://developer.wordpress.org/resource/dashicons
	'category' => 'post-lists',
	'keywords' => ['events', 'list'],
	'mode' => 'preview',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => false,
		'align' => false,
		'customClassName' => false,
		'reusable' => false
	],
	'post_types' => [],
	'render_template' => $block_directory . 'events-list.php',
];
