<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$heading = get_field('heading');
	$subheading = get_field('subheading');
	$text = get_field('text');
	$buttons = get_field('buttons');
?>

<div class="call-to-action">

	<?php if ($heading) : ?>
		<div class="call-to-action__heading"><?php the_field('heading'); ?></div>
	<?php endif; ?>

	<?php if ($subheading) : ?>
		<div class="call-to-action__subheading"><?php the_field('subheading'); ?></div>
	<?php endif; ?>

	<?php if ($text) : ?>
		<div class="call-to-action__text"><?php the_field('text'); ?></div>
	<?php endif; ?>

	<?php if ($buttons) : ?>
		<?php foreach($buttons as $button) : ?>
			<?php if (is_array($button['link'])) : ?>
				<?php echo eswp_format_link($button['link']); ?>
			<?php else : ?>
				<div class="eswp-error-text">Link is a required field.</div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php else : ?>
		<div class="eswp-error-text">Buttons is a required field.</div>
	<?php endif; ?>

</div>