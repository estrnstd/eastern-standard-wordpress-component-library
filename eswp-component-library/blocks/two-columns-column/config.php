<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'two-columns-column';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Two Columns Column'),
	'description' => __('A column.'),
	'icon' => 'table-col-after', // https://developer.wordpress.org/resource/dashicons
	'category' => 'common',
	'keywords' => ['column'],
	'mode' => 'preview',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false,
		'jsx' => true,
	],
	'post_types' => [],
	'render_template' => $block_directory . 'template.php',
	'eswp_data' => [
		'inner_blocks_attributes' => [
			'allowedBlocks' => esc_attr(wp_json_encode([
				'acf/text',
				'acf/image',
				'acf/button-group',
			])),
			'templateLock' => 'false',
		],
	],
];
