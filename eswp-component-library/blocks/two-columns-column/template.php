<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<section class="two-columns__column">
	<?php echo '<InnerBlocks' . eswp_output_attributes($block['eswp_data']['inner_blocks_attributes']) . ' />'; ?>
</section>