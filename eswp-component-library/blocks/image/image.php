<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$image = get_field('image');
	$caption = get_field('caption');
?>

<div class="image">
	<?php if ($image) : ?>
		<?php
			echo eswp_format_acf_picture([
				[
					'field' => 'image',
					'size' => 'example-square-small',
					'media' => '(max-width: 31.25rem)', // 500px
					'pixel_densities' => [2],
				],
				[
					'field' => 'image',
					'size' => 'example-square-large',
					'pixel_densities' => [2],
					'fallback' => true,
				],
			]);
		?>
		<?php if ($caption) : ?>
			<div class="image__caption">
				<?php echo $caption; ?>
			</div>
		<?php endif; ?>
	<?php else : ?>
		<div class="eswp-error-text">Image is a required field.</div>
		<?php if ($is_preview) : ?>
			<div class="eswp-help-text">Click here to add an image.</div>
		<?php endif; ?>
	<?php endif; ?>
</div>