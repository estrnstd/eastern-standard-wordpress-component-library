class AnchorLinks extends HTMLElement {

	constructor() {
		super();
		document.addEventListener('DOMContentLoaded', () => {
			for (const anchorLinkElement of this.anchorLinkElements) {
				anchorLinkElement.addEventListener('click', (event) => {
					event.preventDefault();
					const anchorLinkHref = anchorLinkElement.getAttribute('href');
					if (anchorLinkHref) {
						const anchorPointElement = document.querySelector(anchorLinkHref);
						if (anchorPointElement) {
							anchorPointElement.scrollIntoView({behavior: 'smooth'});
							if (this.updateURL === 'true') {
								const url = new URL(document.URL);
								url.hash = anchorLinkHref;
								document.location.href = url.href;
							}
						}
					}
					return false;
				});
			}
		});
	}

	get anchorLinkElements() {
		return this.querySelectorAll('.anchor-links__link');
	}

	get updateURL() {
		return this.getAttribute('data-update-url');
	}

}

customElements.define('anchor-links', AnchorLinks, {extends: 'section'});
