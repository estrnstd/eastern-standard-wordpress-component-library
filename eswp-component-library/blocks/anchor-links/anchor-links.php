<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$heading = get_field('heading');
	$update_url = get_field('update_url');
	$update_url_output = 'true';
	if (!$update_url) {
		$update_url_output = 'false';
	}
	$anchor_point_names = eswp_get_block_field([
		'field' => 'name',
		'block_name' => 'acf/anchor-point',
		'post' => $post_id,
	]);
	$anchor_point_keys = eswp_get_block_field([
		'field' => 'key',
		'block_name' => 'acf/anchor-point',
		'post' => $post_id,
	]);
?>

<section class="anchor-links" is="anchor-links" data-update-url="<?php echo $update_url_output; ?>">
	<?php if ($heading) : ?>
		<div class="anchor-links__heading"><?php echo $heading; ?></div>
	<?php endif; ?>
	<ul class="anchor-links__list">
		<?php foreach ($anchor_point_names as $index => $anchor_point_name) : ?>
			<?php
				$anchor_point_key = $anchor_point_keys[$index];
				if (!$anchor_point_key) {
					$anchor_point_key = strtolower($anchor_point_name);
					$anchor_point_key = str_replace(' ', '-', $anchor_point_key);
					$anchor_point_key = urlencode($anchor_point_key);
				}
			?>
			<li class="anchor-links__item">
				<a class="anchor-links__link" href="#<?php echo $anchor_point_key; ?>"><?php echo $anchor_point_name; ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</section>
