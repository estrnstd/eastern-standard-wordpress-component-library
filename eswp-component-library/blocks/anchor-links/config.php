<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'anchor-links';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Anchor Links'),
	'description' => __('Example description.'),
	'icon' => 'admin-links',
	'category' => 'common',
	'keywords' => ['anchor', 'links'],
	'mode' => 'auto',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false
	],
	'post_types' => [],
	'render_template' => $block_directory . 'anchor-links.php',
	'enqueue_assets' => function() use($block_name, $block_directory_public) {
		wp_enqueue_style($block_name, $block_directory_public . 'styles/dist/anchor-links.css');
		wp_enqueue_script($block_name, $block_directory_public . 'scripts/dist/anchor-links.js', [
			'web-components-polyfill',
			'smooth-scroll-polyfill'
		]);
	},
];
