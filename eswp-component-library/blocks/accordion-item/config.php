<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'accordion-item';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Accordion Item'),
	'description' => __('A collapsible area of content.'),
	'icon' => 'align-wide', // https://developer.wordpress.org/resource/dashicons
	'category' => 'common',
	'keywords' => ['accordion', 'item', 'collapse', 'collapsible'],
	'mode' => 'preview',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false,
		'jsx' => true,
	],
	'post_types' => [],
	'render_template' => $block_directory . 'accordion-item.php',
	'eswp_data' => [
		'inner_blocks_attributes' => [
			'allowedBlocks' => esc_attr(wp_json_encode([
				'acf/text',
				'acf/button-group',
			])),
			'templateLock' => 'false',
		],
	],
];
