<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
 $heading = get_field('heading');
?>

<li class="accordion__item">
	<button class="accordion__item-heading">
		<?php if ($heading) : ?>
			<?php echo $heading; ?>
		<?php else : ?>
			<div class="eswp-error-text">Accordion Item Heading is a required field.</div>
		<?php endif; ?>
	</button>
	<div class="accordion__item-content">
		<div class="accordion__item-content-inner">
			<?php echo '<InnerBlocks' . eswp_output_attributes($block['eswp_data']['inner_blocks_attributes']) . ' />'; ?>
		</div>
	</div>
</li>
