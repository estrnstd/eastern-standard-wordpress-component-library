<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'accordion';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Accordion'),
	'description' => __('A group of collapsible content.'),
	'icon' => 'menu-alt', // https://developer.wordpress.org/resource/dashicons
	'category' => 'common',
	'keywords' => ['accordion', 'collapse', 'collapsible'],
	'mode' => 'preview',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false,
		'jsx' => true,
	],
	'post_types' => [],
	'render_template' => $block_directory . 'accordion.php',
	'enqueue_assets' => function() use($block_name, $block_directory_public) {
		wp_enqueue_style($block_name, $block_directory_public . 'styles/dist/accordion.css');
		wp_enqueue_script($block_name, $block_directory_public . 'scripts/dist/accordion.js', ['web-components-polyfill']);
	},
	'eswp_data' => [
		'inner_blocks_attributes' => [
			'allowedBlocks' => esc_attr(wp_json_encode([
				'acf/accordion-item',
			])),
			'templateLock' => 'false',
		],
	],
];
