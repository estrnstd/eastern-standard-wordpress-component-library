<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<?php
	$heading = get_field('heading');
?>

<section class="accordion" is="x-accordion">
	<?php if ($heading) : ?>
		<div class="accordion__heading"><?php echo $heading; ?></div>
	<?php endif; ?>
	<ul class="accordion__items">
		<?php echo '<InnerBlocks' . eswp_output_attributes($block['eswp_data']['inner_blocks_attributes']) . ' />'; ?>
	</ul>
</section>