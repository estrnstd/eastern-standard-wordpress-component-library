const AceAccordion = require('./vendor/ace-accordion.js');

class Accordion extends HTMLElement {

	constructor() {
		super();
	}

	connectedCallback() {
		this.aceAccordion = new AceAccordion({
			selectors: {
				accordion: '.accordion__items',
				item: '.accordion__item',
				heading: '.accordion__item-heading',
				content: '.accordion__item-content',
				content_inner: '.accordion__item-content-inner',
			},
		});
	}

}

customElements.define('x-accordion', Accordion, {extends: 'section'});