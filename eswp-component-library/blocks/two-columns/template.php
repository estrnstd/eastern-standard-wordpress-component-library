<?php
/**
 * ACF Block Template.
 *
 * @param  array         $block       The block settings and attributes.
 * @param  string        $content     The block inner HTML (empty).
 * @param  bool          $is_preview  True during AJAX preview.
 * @param  (int|string)  $post_id     The post ID this block is saved to.
 */
?>

<section
	class="two-columns"
	data-column-split="<?php the_field('column_split'); ?>"
	data-left-column-vertical-alignment="<?php the_field('left_column_vertical_alignment'); ?>"
	data-right-column-vertical-alignment="<?php the_field('right_column_vertical_alignment'); ?>"
	data-stacked-order="<?php the_field('stacked_order'); ?>"
>
	<?php echo '<InnerBlocks' . eswp_output_attributes($block['eswp_data']['inner_blocks_attributes']) . ' />'; ?>
</section>