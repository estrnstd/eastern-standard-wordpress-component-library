<?php

/**
 * Block Config.
 *
 * @param  string  $block_directory  A path to this block directory (includes trailing '/').
 * @param  string  $block_directory_public  A public path to this block directory (includes trailing '/').
 */

// https://www.advancedcustomfields.com/resources/acf_register_block_type/

$block_name = 'two-columns';

// Required
$eswp_block_data = [
	'name' => $block_name,
	'title' => __('Two Columns'),
	'description' => __('Two column content layout.'),
	'icon' => 'columns', // https://developer.wordpress.org/resource/dashicons
	'category' => 'common',
	'keywords' => ['columns'],
	'mode' => 'preview',
	'supports' => [
		'align' => false,
		'align_text' => false,
		'align_content' => false,
		'full_height' => false,
		'mode' => false,
		'multiple' => true,
		'align' => false,
		'customClassName' => false,
		'reusable' => false,
		'jsx' => true,
	],
	'post_types' => [],
	'render_template' => $block_directory . 'template.php',
	'enqueue_assets' => function() use($block_name, $block_directory_public) {
		if (is_admin()) {
			wp_enqueue_style($block_name, $block_directory_public . 'styles/dist/block-editor.css');
		}
		wp_enqueue_style($block_name, $block_directory_public . 'styles/dist/theme.css');
	},
	'eswp_data' => [
		'inner_blocks_attributes' => [
			'template' => esc_attr(json_encode([
				[
					'acf/two-columns-column',
				],
				[
					'acf/two-columns-column',
				],
			])),
			'templateLock' => 'true',
		],
	],
];
