<?php

// https://developer.wordpress.org/reference/functions/register_taxonomy

$singular_name = 'Event Type';
$plural_name = 'Event Types';

// Required
$eswp_taxonomy_data = [
	// Required
	'key' => 'event-type',
	// Required
	'object_type' => ['event'],
	// Required
	'arguments' => [
		'description' => 'The type of event.',
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
    'show_in_quick_edit' => false,
    'meta_box_cb' => false,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'show_in_rest' => false,
		'capabilities' => [
			'manage_terms' => 'manage_categories',
			'edit_terms' => 'manage_categories',
			'delete_terms' => 'manage_categories',
			'assign_terms' => 'edit_posts',
		],
		'labels' => [
			'name' => $plural_name,
			'singular_name' => $singular_name,
			'search_items' => "Search {$plural_name}",
			'popular_items' => "Popular {$plural_name}",
			'all_items' => "All {$plural_name}",
			'parent_item' => "Parent {$singular_name}",
			'parent_item_colon' => "Parent {$singular_name}:",
			'name_field_description' => "The name is how it appears on your site.",
			'slug_field_description' => "The \"slug\" is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.",
			'parent_field_description' => "Assign a parent term to create a hierarchy. The term Jazz, for example, would be the parent of Bebop and Big Band",
			'desc_field_description' => "The description is not prominent by default; however, some themes may show it.",
			'edit_item' => "Edit {$singular_name}",
			'view_item' => "View {$singular_name}",
			'update_item' => "Update {$singular_name}",
			'add_new_item' => "Add New {$singular_name}",
			'new_item_name' => "New {$singular_name} name",
			'separate_items_with_commas' => "Separate {$plural_name} with commas",
			'add_or_remove_items' => "Add or remove {$plural_name}",
			'choose_from_most_used' => "Choose from the most used {$plural_name}",
			'not_found' => "No {$plural_name} found",
			'no_terms' => "No {$plural_name}",
			'filter_by_item' => "Filter by {$singular_name}",
			'items_list_navigation' => "Filter by {$singular_name}",
			'item_link' => "{$singular_name} Link",
			'item_link_description' => "A link to a {$singular_name}",
		],
	],
];