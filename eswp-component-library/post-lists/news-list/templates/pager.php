<?php
/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
	$template_class_prefix = 'post-list-news-list';
	$total = $results_object['total_pages']; // The total amount of pages.
	$current = $results_object['page']; // The current page number.
?>

<div data-post-list-template="pagination" class="<?php echo $template_class_prefix; ?>__pagination">

	<?php
		/**
		 * Customizable options.
		 */
		$show_all = false; // Whether to show all pages.
		$end_size = 1; // How many numbers on either the start and the end list edges.
		$mid_size = 2; // How many numbers to either side of the current pages.
		$prev_next = true; // Whether to include the previous and next links in the list.
	?>

	<?php
		/**
		 * Internal variables. DO NOT CHANGE.
		 */
		$previous_page_display = false;
		$spacer_tracker = 0;
	?>

	<?php if ($total > 1) : ?>
		<ul class="<?php echo $template_class_prefix; ?>__pager">

			<?php if ($prev_next && $current > 1) : ?>
				<li class="<?php echo $template_class_prefix; ?>__pager-item">
					<button class="<?php echo $template_class_prefix; ?>__pager-button" data-page="<?php echo $current - 1; ?>">< previous</button>
				</li>
			<?php endif; ?>

			<?php for ($page = 1; $page <= $total; $page++) : ?>
				<?php
					$display_item = false;
					// $show_all
					if ($show_all) {
						$display_item = true;
					}
					// $end_size
					if ($page <= $end_size || $page >= ($total - $end_size + 1)) {
						$display_item = true;
					}
					// $mid_size
					if ($page >= ($current - $mid_size) && $page <= ($current + $mid_size)) {
						$display_item = true;
					}
					// current
					if ($page === $current) {
						$display_item = true;
					}
					// spacer
					if ($display_item != $previous_page_display) {
						$spacer_tracker++;
					}
					$previous_page_display = $display_item;
					if ($spacer_tracker >= 3) {
						$spacer_tracker = 1;
						echo '<li class="' . $template_class_prefix . '__pager-item spacer">...</li>';
					}
					// display page
					if ($display_item) :
				?>
					<li class="<?php echo $template_class_prefix; ?>__pager-item">
						<?php
							$button_class = $template_class_prefix . '__pager-button';
							if ($page == $current) {
								$button_class .= ' current-page';
							}
						?>
						<button class="<?php echo $button_class ?>" data-page="<?php echo $page; ?>"><?php echo $page; ?></button>
					</li>
				<?php endif; ?>
			<?php endfor; ?>

			<?php if ($prev_next && $current < $total) : ?>
				<li class="<?php echo $template_class_prefix; ?>__pager-item">
					<button class="<?php echo $template_class_prefix; ?>__pager-button" data-page="<?php echo $current + 1; ?>">next ></button>
				</li>
			<?php endif; ?>

		</ul>
	<?php endif; ?>

</div>