<?php

/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
$template_key = 'results';
$template_class_prefix = 'post-list-news-list';
$template_class = $template_class_prefix . '-results';
?>

<ul data-post-list-template="<?php echo $template_key; ?>" class="<?php echo $template_class_prefix; ?>__results">
	<?php if (count($results_object['results']) > 0) : ?>
		<?php foreach ($results_object['results'] as $result) : ?>
			<?php
				//
				// type
				//
				$news_type = eswp_get_block_field([
					'field' => 'type',
					'block_name' => 'acf/news-information',
					'post' => $result,
					'first_match' => true,
				]);
				//
				// date
				//
				$date = eswp_get_block_field([
					'field' => 'date',
					'block_name' => 'acf/news-information',
					'post' => $result,
					'first_match' => true,
				]);
			?>
			<li class="<?php echo $template_class_prefix; ?>__result">

				<a
					class="<?php echo $template_class_prefix; ?>__result-title"
					title="<?php echo $result->post_title; ?>"
					href="<?php echo get_permalink($result); ?>"
				>
					<?php echo $result->post_title; ?>
				</a>

				<?php if ($news_type) : ?>
					<div class="<?php echo $template_class_prefix; ?>__result-type">
						<?php echo $news_type->name; ?>
					</div>
				<?php endif; ?>

				<?php if ($date) : ?>
					<div class="<?php echo $template_class_prefix; ?>__result-date">
						<?php echo $date; ?>
					</div>
				<?php endif; ?>

			</li>
		<?php endforeach; ?>
	<?php else : ?>
		<p>No results.</p>
	<?php endif; ?>
</ul>