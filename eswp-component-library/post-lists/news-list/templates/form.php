<?php
/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
	$template_class_prefix = 'post-list-news-list';
?>

<form data-post-list-template="form" class="<?php echo $template_class_prefix; ?>__form">
	<?php
		$news_type_terms = get_terms([
			'taxonomy' => 'news-type',
			'hide_empty' => false,
		]);
	?>
	<label for="news_type">Type</label>
	<select id="news_type" name="news_type">
		<option value="" selected>All Types</option>
		<?php foreach($news_type_terms as $news_type_term) : ?>
			<option value="<?php echo $news_type_term->slug; ?>"><?php echo $news_type_term->name; ?></option>
		<?php endforeach; ?>
	</select>
	<button role="button" type="submit">Filter</button>
</form>