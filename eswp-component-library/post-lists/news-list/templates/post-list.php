<?php
/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options          The post list options.
 * @param  (array)  $results_object   The post list results.
 * @param  (array)  $request_data     The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
	$query_string_filtering = 'false';
	if (isset($additional_data) && isset($additional_data['query_string_filtering']) && $additional_data['query_string_filtering']) {
		$query_string_filtering = 'true';
	}
?>

<div
	class="post-list-news-list"
	is="post-list-news-list"
	data-post-list="<?php echo $options['key']; ?>"
	data-query-string-filtering="<?php echo $query_string_filtering; ?>"
>
	<?php $render_post_list_template('form'); ?>
	<?php $render_post_list_template('results'); ?>
	<?php $render_post_list_template('pagination'); ?>
</div>
