<?php

/**
 * Post List Config.
 *
 * @param  string  $post_list_directory         A path to this post list directory (includes trailing '/').
 * @param  string  $post_list_directory_public  A public path to this post list directory (includes trailing '/').
 */

 $post_list_key = 'news-list';

// Required.
$eswp_post_list_options = [
	'key' => $post_list_key, // Required. Must be unique.
	'post_list_template' => $post_list_directory . 'templates/post-list.php', // Required.
	'templates' => [
		'form' => $post_list_directory . 'templates/form.php',
		'results' => $post_list_directory . 'templates/results.php',
		'pagination' => $post_list_directory . 'templates/load-more.php',
	],
	'enqueue_assets' => function() use($post_list_key, $post_list_directory_public) {
		wp_enqueue_style($post_list_key, $post_list_directory_public . 'styles/dist/news-list.css');
		wp_enqueue_script($post_list_key, $post_list_directory_public . 'scripts/dist/news-list.js', ['eswp-post-list']);
	},
	'posts_per_page' => 10, // Set to zero or negative number for no limit. Must be an integer. Defaults to 10.
	'initial_results' => false, // Set to false to disable getting results for the initial template render.
	'wp_query_args' => function ($request_data) { // Should return WP_Query args.
		return [
			// https://developer.wordpress.org/reference/classes/wp_query/
			'post_type' => 'news',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		];
	},
	'custom_filter' => function ($post, $request_data) { // Should return true or false.
		//
		// Type
		//
		$news_type_filter_result = true;
		$news_type_filter_value = '';
		if (!empty($request_data['news_type'])) {
			$news_type_filter_value = $request_data['news_type'];
		}
		if (is_string($news_type_filter_value) && strlen($news_type_filter_value) > 0) {
			$news_type_filter_result = false;
			$news_type = eswp_get_block_field([
				'field' => 'type',
				'block_name' =>'acf/news-information',
				'post' => $post,
				'first_match' => true,
			]);
			$news_type = $news_type->slug;
			if ($news_type === $news_type_filter_value) {
				$news_type_filter_result = true;
			}
		}
		//
		// Return
		//
		if ($news_type_filter_result) {
			return true;
		}
		else {
			return false;
		}
	},
	'custom_sort' => function ($request_data) { // Should return a compare function.
		return function ($a, $b) {
			$date_a = eswp_get_block_field([
				'field' => 'date',
				'block_name' =>'acf/news-information',
				'post' => $a,
				'first_match' => true,
			]);
			$date_b = eswp_get_block_field([
				'field' => 'date',
				'block_name' =>'acf/news-information',
				'post' => $b,
				'first_match' => true,
			]);
			$timestamp_a = strtotime($date_a);
			$timestamp_b = strtotime($date_b);
			if ($timestamp_a > $timestamp_b) {
				return 1;
			}
			else if ($timestamp_a < $timestamp_b) {
				return -1;
			}
			else {
				return 0;
			}
		};
	},
];