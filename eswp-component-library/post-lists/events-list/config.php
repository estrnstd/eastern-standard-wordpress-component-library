<?php

/**
 * Post List Config.
 *
 * @param  string  $post_list_directory         A path to this post list directory (includes trailing '/').
 * @param  string  $post_list_directory_public  A public path to this post list directory (includes trailing '/').
 */

 $post_list_key = 'events-list';

// Required.
$eswp_post_list_options = [
	'key' => $post_list_key, // Required. Must be unique.
	'post_list_template' => $post_list_directory . 'templates/post-list.php', // Required.
	'templates' => [
		'form' => $post_list_directory . 'templates/form.php',
		'results' => $post_list_directory . 'templates/results.php',
		'pagination' => $post_list_directory . 'templates/load-more.php',
	],
	'enqueue_assets' => function() use($post_list_key, $post_list_directory_public) {
		wp_enqueue_style($post_list_key, $post_list_directory_public . 'styles/dist/events-list.css');
		wp_enqueue_script($post_list_key, $post_list_directory_public . 'scripts/dist/events-list.js', ['eswp-post-list']);
	},
	'posts_per_page' => 10, // Set to zero or negative number for no limit. Must be an integer. Defaults to 10.
	'initial_results' => false, // Set to false to disable getting results for the initial template render.
	'wp_query_args' => function ($request_data) { // Should return WP_Query args.
		return [
			// https://developer.wordpress.org/reference/classes/wp_query/
			'post_type' => 'event',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		];
	},
	'custom_filter' => function ($post, $request_data) { // Should return true or false.
		//
		// Upcoming/Past
		//
		$upcoming_past_filter_result = true;
		$upcoming_past = 'upcoming';
		if (!empty($request_data['events_upcoming_past'])) {
			$upcoming_past = $request_data['events_upcoming_past'];
		}
		$end_date = eswp_get_block_field([
			'field' => 'end_date',
			'block_name' =>'acf/event-information',
			'post' => $post,
			'first_match' => true,
		]);
		if (isset($end_date) && is_string($end_date)) {
			$upcoming_past_filter_result = false;
			$endDateUnixTimestamp = strtotime($end_date);
			$nowUnixTimestamp = strtotime('now');
			if ($upcoming_past == 'upcoming') {
				if ($endDateUnixTimestamp > $nowUnixTimestamp) {
					$upcoming_past_filter_result = true;
				}
			}
			else if ($upcoming_past == 'past') {
				if ($endDateUnixTimestamp <= $nowUnixTimestamp) {
					$upcoming_past_filter_result = true;
				}
			}
		}
		//
		// Type
		//
		$event_type_filter_result = true;
		$event_type_filter_value = '';
		if (!empty($request_data['events_type'])) {
			$event_type_filter_value = $request_data['events_type'];
		}
		if (is_string($event_type_filter_value) && strlen($event_type_filter_value) > 0) {
			$event_type_filter_result = false;
			$event_type = eswp_get_block_field([
				'field' => 'type',
				'block_name' =>'acf/event-information',
				'post' => $post,
				'first_match' => true,
			]);
			$event_type = $event_type->slug;
			if ($event_type === $event_type_filter_value) {
				$event_type_filter_result = true;
			}
		}
		//
		// Return
		//
		if ($upcoming_past_filter_result && $event_type_filter_result) {
			return true;
		}
		else {
			return false;
		}
	},
	'custom_sort' => function ($request_data) { // Should return a compare function.
		$upcoming_past = 'upcoming';
		if (!empty($request_data['upcoming_past'])) {
			$upcoming_past = $request_data['upcoming_past'];
		}
		return function ($a, $b) use($upcoming_past) {
			$start_date_a = eswp_get_block_field([
				'field' => 'start_date',
				'block_name' =>'acf/event-information',
				'post' => $a,
				'first_match' => true,
			]);
			$start_date_b = eswp_get_block_field([
				'field' => 'start_date',
				'block_name' =>'acf/event-information',
				'post' => $b,
				'first_match' => true,
			]);
			$start_timestamp_a = strtotime($start_date_a);
			$start_timestamp_b = strtotime($start_date_b);
			if ($upcoming_past === 'past') {
				if ($start_timestamp_a < $start_timestamp_b) {
					return 1;
				}
				else if ($start_timestamp_a > $start_timestamp_b) {
					return -1;
				}
				else {
					return 0;
				}
			}
			else  {
				if ($start_timestamp_a > $start_timestamp_b) {
					return 1;
				}
				else if ($start_timestamp_a < $start_timestamp_b) {
					return -1;
				}
				else {
					return 0;
				}
			}
		};
	},
];