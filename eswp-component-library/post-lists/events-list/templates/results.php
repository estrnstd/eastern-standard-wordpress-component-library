<?php

/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
$template_key = 'results';
$template_class_prefix = 'post-list-events-list';
$template_class = $template_class_prefix . '-results';
?>

<ul data-post-list-template="<?php echo $template_key; ?>" class="<?php echo $template_class_prefix; ?>__results">
	<?php if (count($results_object['results']) > 0) : ?>
		<?php foreach ($results_object['results'] as $result) : ?>
			<?php
				//
				// type
				//
				$event_type = eswp_get_block_field([
					'field' => 'type',
					'block_name' => 'acf/event-information',
					'post' => $result,
					'first_match' => true,
				]);
				//
				// date
				//
				$start_date = eswp_get_block_field([
					'field' => 'start_date',
					'block_name' => 'acf/event-information',
					'post' => $result,
					'first_match' => true,
				]);
				$end_date = eswp_get_block_field([
					'field' => 'end_date',
					'block_name' => 'acf/event-information',
					'post' => $result,
					'first_match' => true,
				]);
			?>
			<li class="<?php echo $template_class_prefix; ?>__result">

				<a
					class="<?php echo $template_class_prefix; ?>__result-title"
					title="<?php echo $result->post_title; ?>"
					href="<?php echo get_permalink($result); ?>"
				>
					<?php echo $result->post_title; ?>
				</a>

				<?php if ($event_type) : ?>
					<div class="<?php echo $template_class_prefix; ?>__result-type">
						<?php echo $event_type->name; ?>
					</div>
				<?php endif; ?>

				<?php if ($start_date) : ?>
					<div class="<?php echo $template_class_prefix; ?>__result-start-date">
						<?php echo $start_date; ?>
					</div>
				<?php endif; ?>

				<?php if ($end_date) : ?>
					<div class="<?php echo $template_class_prefix; ?>__result-end-date">
						<?php echo $end_date; ?>
					</div>
				<?php endif; ?>

			</li>
		<?php endforeach; ?>
	<?php else : ?>
		<p>No results.</p>
	<?php endif; ?>
</ul>