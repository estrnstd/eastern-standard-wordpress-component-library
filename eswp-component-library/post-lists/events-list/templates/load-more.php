<?php
/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
	$template_class_prefix = 'post-list-events-list';
	$total = $results_object['total_pages']; // The total amount of pages.
	$current = $results_object['page']; // The current page number.
?>

<div data-post-list-template="pagination" class="<?php echo $template_class_prefix; ?>__pagination">

	<?php if ($total > 1 && $current < $total) : ?>
		<button class="<?php echo $template_class_prefix; ?>__load-more" data-page="<?php echo $current + 1; ?>"><?= __('Load More Events') ?></button>
	<?php endif; ?>

</div>