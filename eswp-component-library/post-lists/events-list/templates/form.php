<?php
/**
 * ESWP Post List Template.
 *
 * @param  (array)  $options         The post list options.
 * @param  (array)  $results_object  The post list results.
 * @param  (array)  $request_data    The post list request data.
 * @param  (array)  $additional_data  The value passed into the second parameter of eswp_post_list
 */
?>

<?php
	$template_class_prefix = 'post-list-events-list';
?>

<form data-post-list-template="form" class="<?php echo $template_class_prefix; ?>__form">
	<label for="events_upcoming_past">Upcoming/Past</label>
	<select id="events_upcoming_past" name="events_upcoming_past">
		<option value="upcoming" selected>Upcoming</option>
		<option value="past">Past</option>
	</select>
	<?php
		$event_type_terms = get_terms([
			'taxonomy' => 'event-type',
			'hide_empty' => false,
		]);
	?>
	<label for="events_type">Type</label>
	<select id="events_type" name="events_type">
		<option value="" selected>All Types</option>
		<?php foreach($event_type_terms as $event_type_term) : ?>
			<option value="<?php echo $event_type_term->slug; ?>"><?php echo $event_type_term->name; ?></option>
		<?php endforeach; ?>
	</select>
	<button role="button" type="submit">Filter</button>
</form>