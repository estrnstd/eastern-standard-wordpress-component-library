class EventsList extends ESWPPostList {

	constructor() {
		super();

		this.formAttachEventListeners();
		this.addUpdateTemplateElementCallback('form', this.formAttachEventListeners);
		this.paginationAttachEventListeners();
		this.addUpdateTemplateElementCallback('pagination', this.paginationAttachEventListeners);

		if (this.queryStringFiltering) {
			const queryStrings = this.constructor.getQueryStrings();
			this.setFormValues(queryStrings);
		}
	}

	connectedCallback() {
		this.formSubmitButton.click();
	}

	get queryStringFiltering() {
		const queryStringFilteringAttribute = this.getAttribute('data-query-string-filtering');
		if (!this.constructor.isAdmin() && queryStringFilteringAttribute == 'true') {
			return true;
		}
		else {
			return false;
		}
	}

	getFormValues() {
		return this.constructor.getFormValues(this.templateElements.form);
	}

	setFormValues(values) {
		return this.constructor.setFormValues(this.templateElements.form, values);
	}

	get formSubmitButton() {
		return this.templateElements.form.querySelector('[type="submit"]');
	}

	formAttachEventListeners = () => {
		this.templateElements.form.addEventListener('submit', this.formSubmitHandler);
	}

	formSubmitHandler = (event) => {
		if (event.preventDefault) {
			event.preventDefault();
		}
		this.submitForm();
		return false;
	}

	async submitForm() {
		const formData = this.getFormValues();
		const args = {
			key: this.key,
			templates: ['results', 'pagination'],
			request_data: formData,
			page: 1
		};
		if (this.queryStringFiltering) {
			this.constructor.setQueryStrings(formData);
		}
		const newTemplates = await this.fetchTemplates(args);
		this.updateTemplateElement('results', newTemplates.results);
		this.updateTemplateElement('pagination', newTemplates.pagination);
	}
	
	paginationAttachEventListeners = () => {
		const pagerButtonElements = this.templateElements.pagination.querySelectorAll('button');
		for (let buttonElement of pagerButtonElements) {
			buttonElement.addEventListener('click', this.paginationButtonClickHandler);
		}
	}

	paginationButtonClickHandler = (event) => {
		const page = event.target.getAttribute('data-page');
		this.appendPage(page);
	}

	async getPage(page) {
		let formData = {};
		// Get the most recent request form data.
		// This allows us to revert the form fields to the previously requested values.
		for (let i = this.requestsAndResponses.length - 1; i >= 0; i--) {
			const requestResponse = this.requestsAndResponses[i];
			if (requestResponse.request.action === 'eswp_render_templates') {
				formData = requestResponse.request.args.request_data;
				break;
			}
		}
		this.setFormValues(formData);
		const args = {
			key: this.key,
			templates: ['results', 'pagination'],
			request_data: formData,
			page: page
		};
		const newTemplates = await this.fetchTemplates(args);
		return newTemplates;
	}

	async goToPage(page) {
		const newTemplates = await this.getPage(page);
		this.updateTemplateElement('results', newTemplates.results);
		this.updateTemplateElement('pagination', newTemplates.pagination);
	}

	async appendPage(page) {
		const newTemplates = await this.getPage(page);
		const resultsContainer = this.querySelector('[data-post-list-template="results"]');
		const htmlObject = document.createElement('div');
		htmlObject.innerHTML = newTemplates.results;
		const newResultsItems = htmlObject.querySelectorAll('[data-post-list-template="results"] > li');
		for (const newResultsItem of newResultsItems) {
			resultsContainer.append(newResultsItem);
		}
		this.updateTemplateElement('pagination', newTemplates.pagination);
	}

}

customElements.define('post-list-events-list', EventsList, {extends: 'div'});