<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'anchor_links',
	'title' => 'Anchor Links',
	'fields' => [
		[
			'label' => 'Heading',
			'name' => 'heading',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		],
		[
			'label' => '',
			'name' => 'anchor_links_message',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'message' => 'Anchor Point blocks added within this post will automatically display in this block in the order they appear in the post.',
			'new_lines' => '',
			'esc_html' => 0,
		],
		[
			'label' => 'Update URL',
			'name' => 'update_url',
			'type' => 'true_false',
			'instructions' => 'Disabling URL updates will prevent the anchor point key from being appended to the url when an anchor link is clicked.',
			'required' => 0,
			'message' => '',
			'default_value' => 1,
			'ui' => 1,
			'ui_on_text' => 'On',
			'ui_off_text' => 'Off',
		]
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/anchor-links',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
