<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'accordion_item',
	'title' => 'Accordion Item',
	'fields' => [
		[
			'label' => 'Heading',
			'name' => 'heading',
			'type' => 'text',
			'instructions' => 'This heading will display even when the item is collapsed. Clicking on this heading will open and close the accordion item.',
			'required' => 1,
			'default_value' => 'Example accordion item heading',
			'placeholder' => '',
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/accordion-item',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
