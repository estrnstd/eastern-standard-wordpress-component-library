<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'call_to_action',
	'title' => 'Call To Action',
	'fields' => [
		[
			'label' => 'Heading',
			'name' => 'heading',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		],
		[
			'label' => 'Subheading',
			'name' => 'subheading',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		],
		[
			'label' => 'Text',
			'name' => 'text',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'default_value' => '',
			'tabs' => 'visual',
			'toolbar' => 'eswp_full',
			'media_upload' => 0,
			'delay' => 0,
		],
		[
			'label' => 'Buttons',
			'name' => 'buttons',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 1,
			'collapsed' => '',
			'min' => 1,
			'max' => 2,
			'layout' => 'block',
			'button_label' => 'Add Link',
			'sub_fields' => [
				[
					'label' => 'Link',
					'name' => 'link',
					'type' => 'link',
					'instructions' => '',
					'required' => 1,
					'return_format' => 'array',
				],
			],
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/call-to-action',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
