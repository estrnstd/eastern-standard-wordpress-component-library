<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'anchor_point',
	'title' => 'Anchor Point',
	'fields' => [
		[
			'label' => 'Name',
			'name' => 'name',
			'type' => 'text',
			'instructions' => 'Choose a name for the anchor link. The name will be the link text in the Anchor Links block.',
			'required' => 1,
			'default_value' => '',
			'placeholder' => '',
		],
		[
			'label' => 'Custom Key',
			'name' => 'key',
			'type' => 'text',
			'instructions' => 'Set a custom key for the anchor link. If left blank, a url-safe version of the name will be used. The key will be used as the ID for the anchor point. The key should be a valid id (<a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/id" target="_blank">what is a valid id?</a>).',
			'required' => 0,
			'default_value' => '',
			'placeholder' => '',
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/anchor-point',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
