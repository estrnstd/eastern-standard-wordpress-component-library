<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'button_group',
	'title' => 'Button Group',
	'fields' => [
		[
			'label' => 'Buttons',
			'name' => 'buttons',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 1,
			'layouts' => [
				'button_group__link' => [
					'name' => 'link',
					'label' => 'Link',
					'display' => 'block',
					'sub_fields' => [
						[
							'label' => 'Link',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 1,
							'return_format' => 'array',
						],
					],
					'min' => '',
					'max' => '',
				],
				'button_group__download' => [
					'name' => 'download',
					'label' => 'Download',
					'display' => 'block',
					'sub_fields' => [
						[
							'label' => 'Button Text',
							'name' => 'button_text',
							'type' => 'text',
							'instructions' => 'If left blank, the file name will be used.',
							'required' => 0,
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						],
						[
							'label' => 'File',
							'name' => 'file',
							'type' => 'file',
							'instructions' => '',
							'required' => 1,
							'return_format' => 'array',
							'library' => 'all',
							'min_size' => '',
							'max_size' => '',
							'mime_types' => '',
						],
					],
					'min' => '',
					'max' => '',
				],
			],
			'button_label' => 'Add Button',
			'min' => 1,
			'max' => '',
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/button-group',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
