<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'image',
	'title' => 'Image',
	'fields' => [
		[
			'label' => 'Image',
			'name' => 'image',
			'type' => 'image_aspect_ratio_crop',
			'instructions' => 'Minimum required image dimensions: 1000px width by 1000px height. Recommended image dimensions to support higher pixel density screens: 2000px width by 200px height.',
			'required' => 1,
			'crop_type' => 'aspect_ratio',
			'aspect_ratio_width' => 1,
			'aspect_ratio_height' => 1,
			'return_format' => 'array',
			'preview_size' => 'example-square-preview',
			'library' => 'all',
			'min_width' => 500,
			'min_height' => 500,
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		],
		[
			'label' => 'Caption',
			'name' => 'caption',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'default_value' => '',
			'tabs' => 'visual',
			'toolbar' => 'eswp_simple',
			'media_upload' => 0,
			'delay' => 0,
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/image',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
