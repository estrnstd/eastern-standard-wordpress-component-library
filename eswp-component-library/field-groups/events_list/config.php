<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'events_list',
	'title' => 'Events List',
	'fields' => [
		[
			'label' => 'Query String Filtering',
			'name' => 'query_string_filtering',
			'type' => 'true_false',
			'instructions' => 'Enabling query string filtering will sync form filters with query string values. This can be useful for linking directly to a pre-filtered list. Note: having more than one component on a page with this feature enabled may cause issues.',
			'required' => 0,
			'message' => '',
			'default_value' => 0,
			'ui' => 1,
			'ui_on_text' => 'On',
			'ui_off_text' => 'Off',
		]
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/events-list',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
