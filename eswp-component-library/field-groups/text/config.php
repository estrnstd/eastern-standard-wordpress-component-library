<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'text',
	'title' => 'Text',
	'fields' => [
		[
			'label' => 'Text',
			'name' => 'text',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 1,
			'default_value' => '',
			'tabs' => 'visual',
			'toolbar' => 'eswp_full',
			'media_upload' => 0,
			'delay' => 0,
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/text',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
