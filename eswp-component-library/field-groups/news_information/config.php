<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'news_information',
	'title' => 'News Information',
	'fields' => [
		[
			'label' => 'Date',
			'name' => 'date',
			'type' => 'date_time_picker',
			'instructions' => 'Choose a date for the news.',
			'required' => 1,
			'display_format' => 'm/d/Y g:i a',
			'return_format' => 'm/d/Y g:i a',
			'first_day' => 1,
		],
		[
			'label' => 'Type',
			'name' => 'type',
			'type' => 'taxonomy',
			'instructions' => 'Select a news type. You can add/edit news types here: <a href="/wp-admin/edit-tags.php?taxonomy=news-type&post_type=news" target="_blank">/wp-admin/edit-tags.php?taxonomy=news-type&post_type=news</a>',
			'required' => 0,
			'taxonomy' => 'news-type',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'multiple' => 0,
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/news-information',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
