<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
$eswp_field_group_data = [
	'key' => 'event_information',
	'title' => 'Event Information',
	'fields' => [
		[
			'label' => 'Start Date',
			'name' => 'start_date',
			'type' => 'date_time_picker',
			'instructions' => 'Choose a start date.',
			'required' => 1,
			'display_format' => 'm/d/Y g:i a',
			'return_format' => 'm/d/Y g:i a',
			'first_day' => 1,
		],
		[
			'label' => 'End Date',
			'name' => 'end_date',
			'type' => 'date_time_picker',
			'instructions' => 'Choose an end date.',
			'required' => 1,
			'display_format' => 'm/d/Y g:i a',
			'return_format' => 'm/d/Y g:i a',
			'first_day' => 1,
		],
		[
			'label' => 'Type',
			'name' => 'type',
			'type' => 'taxonomy',
			'instructions' => 'Select an event type. You can add/edit event types here: <a href="/wp-admin/edit-tags.php?taxonomy=event-type&post_type=event" target="_blank">/wp-admin/edit-tags.php?taxonomy=event-type&post_type=event</a>',
			'required' => 0,
			'taxonomy' => 'event-type',
			'field_type' => 'select',
			'allow_null' => 1,
			'add_term' => 0,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'object',
			'multiple' => 0,
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/event-information',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
