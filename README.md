# Eastern Standard WordPress Component Library
SHOULD NOT BE ACTIVATED EXCEPT TO TEST FEATURES. Activating this plugin will enable all component library features for testing purposes. It's purpose is help to determine the capabilities of a component library feature. The plugin is not something that should be relied on for real functionality of your site. After you're done testing, copy the desired code out of this plugin into your theme, and deactivate this plugin.

## Requirements
This plugin requires these plugins to be installed and activated:

* <a href="https://www.advancedcustomfields.com/pro" target="_blank">Advanced Custom Fields Pro</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities" target="_blank">Eastern Standard WordPress Utilities</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-type-framework" target="_blank">Eastern Standard WordPress Post Type Framework</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-block-framework" target="_blank">Eastern Standard WordPress Block Framework
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-taxonomy-framework" target="_blank">Eastern Standard WordPress Taxonomy Framework</a>
* <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework" target="_blank">Eastern Standard WordPress Field Group Framework</a>

## Installation
Copy the contents of the `eswp-component-library` directory from this repository into the plugins directory. The path should look like this: `/wp-content/plugins/eswp-component-library/eswp-component-library.php`.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Updates
This plugin can be updated from the WordPress dashboard. It is recommended that you update this plugin using the WordPress dashboard because it is less error-prone than updating it manually.

If a plugin update has been pushed to this repository, but you do not see the update available in your WordPress dashboard, follow these steps:

1. Make sure the `allow_dashboard_updating` variable is set to `true`. Check your dashboard again after changing the variable.
2. If the update is still not appearing, it is probably caching the update request (this cache is cleared automatically every 24 hours). Disable update caching for this plugin by setting the `cache_allowed` variable to `false`. Check your dashboard again after changing the variable.
3. If the problem persists, delete the plugin and follow the installation steps in this readme.

If after a successful update, the plugin still shows a new update available, check the recent commits and ensure the plugin version was updated properly in all locations detailed in the <a href="#markdown-header-development">Development</a> section of this readme.

As the development of a project comes to a close, if the website owner will not be an ongoing client with Eastern Standard, it is our policy to set the `allow_dashboard_updating` variable to `false`. This will prevent the plugin from searching for new versions in the Wordpress dashboard. This is our policy for a few reasons:

* This prevents clients from updating our plugin through the dashboard, potentially causing unintended effects on their website.
* This prevents our plugin from appearing out of date to clients that like to keep plugins updated.
* The client is no longer paying us for our work so they will not benefit from our continued development of this plugin.

## Development

When committing updates, remember to update the version in `eswp-component-library/eswp-component-library.php` and `info.json`, then compress the `eswp-component-library` directory into a new `eswp-component-library.zip` file. These steps ensure te WordPress plugin update process will work as expected.